package Day_4.Day_4;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
public class Alert1{
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\jnikhil001\\eclipse-workspace\\Day_4\\drivers\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
         
        driver.get("https://the-internet.herokuapp.com/");
        driver.manage().window().maximize();
        
        driver.findElement(By.xpath("//a[text()='JavaScript Alerts']")).click();
        
        driver.findElement(By.xpath("//button[text()='Click for JS Alert']")).click();
        
        Alert alert = driver.switchTo().alert();
        
        System.out.println(alert.getText());
        
        alert.accept();
        
        
        
    }
}