package Day_4.Day_4;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
public class iframe{
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\jnikhil001\\eclipse-workspace\\Day_4\\drivers\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
         
        driver.get("https://the-internet.herokuapp.com/");
        
        //driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        
        driver.findElement(By.xpath("//a[text()='Frames']")).click();
        
        driver.findElement(By.xpath("//a[text()='iFrame']")).click();
        
        WebElement frames = driver.findElement(By.xpath("//iframe[@id='mce_0_ifr']"));
        driver.switchTo().frame(frames);
        
        WebElement val = driver.findElement(By.xpath("//body[@id='tinymce']"));
        val.clear();
        
        val.sendKeys("Hello!!..This is Nikhil");
        
    }
}